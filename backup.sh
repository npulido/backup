#!/bin/bash
IFS=$'\n'
if [ $# == 0 ]; then
        echo "$1 $# - No arguments given."
        exit 1
elif [ ! -f $1 ]; then
        echo "$1 - Argument is not a valid file."
        exit 1
fi

filename=$1

if [ -z "$2" ]
then
	desc=$(date +"%F_%H%M%S")
else
	desc=$2
fi

file_len=${#filename}
count=1
while [ $count -le $file_len ]
do
	char=${filename: -$count:1}
	if [ $char == '.' ]
	then
		atom=${filename:0: -$count}
		suffix=${filename: -$count}
		break
	else
		count=$((count + 1))
	fi
done
#echo char $char file_len $file_len count $count atom $atom suffix $suffix

if [ -z $atom ]
then
	atom=$filename
fi

mkdir -p bak

files=0
for file in bak/$atom-*
do
	if [ -e $file ]
	then 
		files=$((files + 1))
	fi
done
number=$(($files + 1))

recursion=0
function cp_and_tar 
{
	local number=$(printf "%02d" $1)
	
	if [ ! -e bak/$atom-$number-$desc.tar.xz ] && [ $recursion -lt 100 ] 
	then
		cp $filename bak/$atom-$number-$desc$suffix
		cd bak
		tar -cJvf $atom-$number-$desc.tar.xz $atom-$number-$desc$suffix
		rm $atom-$number-$desc$suffix
	else
		recursion=$((recursion + 1))
		cp_and_tar $((number + 1))
	fi
}

cp_and_tar $number
